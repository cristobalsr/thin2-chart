import { Component, ElementRef, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';

const Highcharts = require('./dependencies/highcharts');
//const Highcharts_3d = require('./dependencies/highcharts-3d');
//const Highstock = require('./dependencies/highstock');

@Component({
    selector: 'th-ui-chart',
    template: '<div #th_ui_chart><div>',
    inputs: ['options', 'type']
})
export class ThUiChart implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('th_ui_chart')
    private _thChart: ElementRef;
    private _options: any;
    private _type: string;

    public get thChart(): any {
        return this._thChart;
    }

    public set thChart(value: any) {
        this._thChart = value;
    }

    public get options(): any {
        return this._options;
    }

    public set options(value: any) {
        this._options = value;
    }

    public get type(): string {
        return this._type;
    }

    public set type(value: string) {
        this._type = value;
    }

    ngOnInit() { }

    ngAfterViewInit() {
        this.draw();
    }

    draw(): any {
        if (!this.options || !this.type)
            throw new Error('Object option\'s or chart type is incorrect');

        let arrayKeys = Object.keys(this.options);

        if (arrayKeys.length <= 2)
            throw new Error('Minimum configuration required');

        if (this.thChart && this.thChart.nativeElement) {
            this.options.chart = {
                type: this.type,
                renderTo: this.thChart.nativeElement
            };
            this.thChart = new Highcharts.Chart(this.options);
        } else
            throw new Error('chart element is null or undefined');

        return this.thChart;
    }

    destroy(): void {
        if (this.thChart)
            this.thChart.destroy();
    }

    ngOnDestroy() {
        this.destroy()
    }

} 